/*

slack-notifier

https://bitbucket.org/jerryb/slack-notifier

Usage:

var slack = require('slack-notifier')(MY_SLACK_WEBHOOK_URL);

slack.alert('Something bad happened!');

slack.send({
  channel: '#myCustomChannelName',
  icon_url: 'http://example.com/my-icon.png',
  text: 'Here is my notification',
  unfurl_links: 1,
  username: 'myapp'
});

*/

var request = require('request');
var _ = require('lodash');


module.exports = function (options) {
    var notify = {},
        _env = process.env.NODE_ENV,
        _config =  (_.isString(options)) ? { url: options } : options;


  notify.request = function (data, done) {
    if (!_config.url) {
      console.log('No WebHook URL configured.');
      return false;
    }

    if (!_.isFunction(done)) {
      done = _.noop;
    }
    if (!_.isFunction(notify.onError)) {
      notify.onError = _.noop;
    }

    request.post(_config.url, {
      form: {
        payload: JSON.stringify(data)
      }
    }, function(err, response) {
      if (err) {
        notify.onError(err);
        return done(err);
      }
      if (response.body !== 'ok') {
        notify.onError(new Error(response.body));
        return done(new Error(response.body));
      }

      done();
    });
  };

  notify.send = function (options, done) {
    if (_.isString(options)) {
      options = { text: options };
    }

    // Merge options with defaults
    var defaults = {
      channel: '#general',
      username: 'Bot',
      text: '',
      icon_emoji: ':bell:'
    };
    //Add the environment to the message if not production
    if (_env !== 'production') {
        options.text = "[" + _env + "] " + options.text;
    }
    var data = _.assign(defaults, options);

    // Move the fields into attachments
    if (options.fields) {
      if (!data.attachments) {
        data.attachments = [];
      }

      data.attachments.push({
        fallback: 'Alert details',
        fields: _.map(options.fields, function (value, title) {
          return {
            title: title,
            value: value,
            short: (value + '').length < 25
          };
        })
      });

      delete(data.fields);
    }

    // Remove the default icon_emoji if icon_url was set in options. Otherwise the default emoji will always override the url
    if (options.icon_url && !options.icon_emoji) {
      delete(data.icon_emoji);
    }

    notify.request(data, done);
  };

  notify.extend = function (defaults) {
    return function (options, done) {
      if (_.isString(options)) {
        options = { text: options };
      }

      notify.send(_.extend(defaults, options), done);
    };
  };


  notify.alert = notify.extend({
    channel: '#alerts',
    icon_emoji: ':warning:',
    username: 'Alert'
  });

  return notify;
};
